//
//  ViewController.swift
//  CmdBaseball
//
//  Created by negaipro on 2016. 7. 17..
//  Copyright © 2016년 negaipro. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{

    @IBOutlet weak var tf1: UITextField!
    @IBOutlet weak var tf2: UITextField!
    @IBOutlet weak var tf3: UITextField!
    @IBOutlet weak var resultLab: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    @IBAction func runBtnAction(_ sender: AnyObject)
    {
        if tf1.text?.characters.count == 0 ||
        tf2.text?.characters.count == 0 ||
        tf3.text?.characters.count == 0
        {
            resultLab.text = "모두 입력 해주세요."
            return
        }
        
        // val1, val2, val3 은 첫번쨰, 두번째, 세번째 들어가는 값을 Int 형으로 변환한 값.
        let val1 = Int(self.tf1.text!)
        let val2 = Int(self.tf2.text!)
        let val3 = Int(self.tf3.text!)
        
        // 계산 하시오.
        
        
        // 결과 출력
        
        resultLab.text = "0S 0B 3Out"
    }

}

